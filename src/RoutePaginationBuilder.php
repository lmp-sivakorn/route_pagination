<?php
namespace LMPSolution\RoutePagination;
use \Illuminate\Pagination\Paginator;

class RoutePaginationBuilder extends \Illuminate\Database\Eloquent\Builder

{
    /**
     * Rewrite Route Paginate the given query.
     *
     * @param  int  $perPage
     * @param  array  $columns
     * @param  string  $pageName
     * @param  int|null  $page
     * @return \lmpsolution\RoutePagination\RoutePaginationAwarePaginator
     *
     * @throws \InvalidArgumentException
     */
    public function routePaginate($perPage = null, $columns = ['*'], $pageName = 'page', $page = null, $routeName=null)
    {
        $page = $page ?: Paginator::resolveCurrentPage($pageName);

        $perPage = $perPage ?: $this->model->getPerPage();

        $results = ($total = $this->toBase()->getCountForPagination())
                                    ? $this->forPage($page, $perPage)->get($columns)
                                    : $this->model->newCollection();

        return new RoutePaginator($results, $total, $perPage, $page, [
            'path' => Paginator::resolveCurrentPath(),
            'pageName' => $pageName,
        ]);
    }
}